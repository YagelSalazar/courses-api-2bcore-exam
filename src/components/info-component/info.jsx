import React from 'react'

const Info = (props) => {

    const { course } = props.location.state
    console.log(course)

    return (
        <div className='info-container'>
            <h1>{course.title}</h1>
            <h2>{course.description}</h2>
            <h3>{`Category: ${course.category}`}</h3>
            <h3>Objetives:</h3>
            <ul>
                {
                    //console.log(course.objectives)
                    (course.objectives).map((obj) => (
                        <li>{obj}</li>
                    ))
                }
            </ul>
            <h3>Prerequisites:</h3>
            <ul>
                {
                    //console.log(course.objectives)
                    (course.prerequisites).map((pr) => (
                        <li>{pr}</li>
                    ))
                }
            </ul>

        </div>
    )
}

export default Info