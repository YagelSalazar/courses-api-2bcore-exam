import React, { Component } from 'react'
import axios from 'axios'
import './courses-styles.css'
import {Link} from 'react-router-dom'

class Courses extends Component {

    state = { courses: [] }

    componentDidMount() {
        axios.post('http://54.211.18.121:3000/courses/getCourses')
            .then((response) => response.data)
            .then((data) => {
                this.setState({ courses: data.data.content })
            })
            .catch((error) => {
                console.log(error)
            })
    }

    render() {

        const { courses } = this.state
        console.log(courses)

        return (
            <div className='container'>
                <h1>Join to our community and learn from all these courses we have for you...</h1>
                <div className='courses-content'>
                    {Object.keys(courses).map((course) => (
                        //<label>{courses[course].title}</label>
                        <div className='course-card'>
                            <h3><Link
                                to={{
                                    pathname: 'info',
                                    state: {
                                        course: courses[course]
                                    }
                                }}
                            >{courses[course].title}</Link></h3>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

export default Courses