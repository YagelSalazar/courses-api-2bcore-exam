import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
//---------------------Styles
import './App.css';
//---------------------Components
import Courses from "./components/courses-component/courses";
import Info from './components/info-component/info'

class App extends Component {

  constructor(props){
    super(props)
    this.state = {courses: []}
  }

  render(){
    return(
      <Router>
        <Switch>
          <Route path='/' component={Courses} exact></Route>
          <Route path='/info' component={Info}></Route>
        </Switch>
      </Router>
    )
  }
}

export default App;
